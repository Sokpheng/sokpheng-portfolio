import React from "react";
import Hero from "@components/Home/Hero";
import { Box, Container } from "@mui/material";
import Story from "@components/Home/Story";
import DevelopmentProgress from "@components/Home/DevelopmentProgress";
import Resume from "@components/Home/Resume";
import Contact from "@components/Home/Contact";

const Home: React.FC = () => {
  return (
    <>
      <Hero />
      <Story />
      <DevelopmentProgress />
      <Resume/>
      <Contact/>
    </>
  );
};

export default Home;
