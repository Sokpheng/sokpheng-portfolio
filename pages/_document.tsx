import Document, { DocumentContext, DocumentInitialProps, Html, Main, NextScript, Head } from "next/document";
import { useTheme as theme } from "@mui/system";
import { ServerStyleSheets } from "@mui/styles";
import React from "react";

class CustomDocument extends Document {
  static async getInitialProps(
    ctx: DocumentContext
  ): Promise<DocumentInitialProps> {
    const sheets = new ServerStyleSheets();
    const originalRenderPage = ctx.renderPage;

    ctx.renderPage = () =>
      originalRenderPage({
        enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
      });

    const initialProps = await Document.getInitialProps(ctx);

    return {
      ...initialProps,
      // Styles fragment is rendered after the app and page rendering finish.
      styles: [...React.Children.toArray(initialProps.styles), sheets.getStyleElement()],
    };
    
  }
  render() {
    return (
      <Html lang="en">
        <Head>
          {/* meta robots */}
          <meta name="robots" content="all" />
          <meta
            name="description"
            content={process.env.NEXT_PUBLIC_APP_DESCRIPTION || ""}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default CustomDocument;
