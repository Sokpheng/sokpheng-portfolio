declare module "react-typical" {
  interface TypicalProps {
    steps: (string | number)[];
    loop: number;
    wrapper: React.ReactNode;
  }

  declare const Typical: React.FC<TypicalProps>;

  export default Typical;
}
