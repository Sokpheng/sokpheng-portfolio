import { IconType } from "react-icons";

export interface IMenuItem {
  id: number;
  label: string;
  icon: React.ReactElement<IconType>;
}

interface IResume {
  id: number;
  title: string;
  year?: string;
}

export interface IEducation extends IResume {
  major: string;
  location: string;
}

interface IPosition {
  id: number;
  title: string;
  responsibilities: string[];
}

export interface IExperience extends IResume {
  location: string;
  position: IPosition[];
}

interface ILevel {
  title: string;
  score: number;
}

export class SkillLevel {
  static readonly BEGINNER: ILevel = { title: "Beginner", score: 40 };
  static readonly INTERMEDIATE: ILevel = { title: "Intermediate", score: 60 };
  static readonly ADVANCE: ILevel = { title: "Advance", score: 80 };
  static readonly EXPERT: ILevel = { title: "Expert", score: 100 };
}

export interface IProgrammingLanguages {
  id: number;
  name: string;
  level: ILevel
}

export interface IProject extends IResume {
  description: string;
  website?: {
    url: string;
    text: string;
  }[];
  technologies: string[];
}

export interface IInterest extends IResume {}

export interface IResumeData {
  education: IEducation[];
  experience: IExperience[];
  programmingLanguages: IProgrammingLanguages[];
  projects: IProject[];
  interests: IInterest[];
}
