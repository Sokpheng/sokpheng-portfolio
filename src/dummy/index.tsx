import React from "react";
import {
  IEducation,
  IExperience,
  IInterest,
  IMenuItem,
  IProgrammingLanguages,
  IProject,
  IResumeData,
  SkillLevel,
} from "@interfaces";
import { FaHistory } from "react-icons/fa";
import { BiCodeAlt } from "react-icons/bi";
import { AiOutlineFundProjectionScreen } from "react-icons/ai";
import { MdPalette } from "react-icons/md";
import { MdSchool } from "react-icons/md";

export const menuData: IMenuItem[] = [
  {
    id: 1,
    label: "Education",
    icon: <MdSchool size={24} />,
  },
  {
    id: 2,
    label: "Work History",
    icon: <FaHistory size={24} />,
  },
  {
    id: 3,
    label: "Programming Skills",
    icon: <BiCodeAlt size={24} />,
  },
  {
    id: 4,
    label: "Projects",
    icon: <AiOutlineFundProjectionScreen size={24} />,
  },
  {
    id: 5,
    label: "Interests",
    icon: <MdPalette size={24}/>,
  },
];

const educationData: IEducation[] = [
  {
    id: 1,
    title: "Step IT Academy Cambodia",
    major: "Software development",
    location: "Phnom Penh",
    year: "2019 - Present",
  },
  {
    id: 2,
    title: "Royal University Of Agriculture",
    major: "Agri-science",
    location: "Phnom Penh",
    year: "2013 - 2018",
  },
  {
    id: 3,
    title: "Bak Touk High School",
    major: "High school",
    location: "Phnom Penh",
    year: "2013",
  },
];

const experience: IExperience[] = [
  {
    id: 1,
    title: "CAMIS Solution co. ltd, Phnom Penh",
    position: [
      {
        id: 1,
        title: "Fullstack Web Developer",
        responsibilities: ["Front-end Developer (React JS, Next JS)", 'Backend (Nest JS)', "UX/UI Designer"],
      },
      {
        id: 2,
        title: "Internship",
        responsibilities: ["Mobile Development (React Native)"],
      },
    ],
    location: "Phnom Penh",
    year: "2020 - 2021",
  },
];


const programmingLanguages: IProgrammingLanguages[] = [
  {
    id: 1,
    name: "React, Next JS",
    level: SkillLevel.ADVANCE,
  },
  {
    id: 3,
    name: "Vuejs 3, Nuxt 3",
    level: SkillLevel.ADVANCE,
  },
  {
    id: 4,
    name: "Javasript",
    level: SkillLevel.ADVANCE,
  },
  {
    id: 5,
    name: "TypeScript",
    level: SkillLevel.INTERMEDIATE,
  },
  {
    id: 6,
    name: "HTML, CSS",
    level: SkillLevel.ADVANCE,
  },
  {
    id: 7,
    name: "Nest JS",
    level: SkillLevel.INTERMEDIATE,
  },
];

const projects: IProject[] = [
  {
    id: 1,
    title: "Personal Portfolio Website",
    technologies: [
      "React JS",
      "Next JS",
      "Styled Components",
      "TypeScript",
    ],
    description:
      "A personal website to showcase all my details and projects ar one place.",
    website: [
      {
        url: "https://sokpheng.vercel.app",
        text: "Website",
      },
      {
        url: "https://sokpheng.vercel.app",
        text: "Git",
      },
    ],
    year: "2022",
  },
];

const interests: IInterest[] = [
  {
    id: 1,
    title: "Coding",
  },
  {
    id: 2,
    title: "Watching Onlione Tutorials",
  },
  {
    id: 3,
    title: "Music",
  },
  {
    id: 4,
    title: "Online Game",
  },
];

export const resumeData: IResumeData = {
  education: educationData,
  experience: experience,
  programmingLanguages: programmingLanguages,
  projects: projects,
  interests: interests,
};
