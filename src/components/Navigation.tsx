import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import {
  colors,
  Container,
  List,
  ListItem,
  ListItemIcon,
  SxProps,
} from "@mui/material";
import { styled, alpha } from "@mui/material/styles";
import { makeStyles } from "@mui/styles";
import { useTheme } from "@mui/material/styles";
import { blue } from "@mui/material/colors";
import Link from "next/link";
import { routes } from "@constants/routes";
import { useRouter } from "next/router";
import clsx from "clsx";
import { MdMenu } from "react-icons/md";
import { AiFillFacebook, AiFillGitlab } from "react-icons/ai";
import { BsTelegram } from "react-icons/bs";

const useStyles = makeStyles((theme) => ({
  logo: {
    flexGrow: 1,
    "&:hover": {
      transform: "scale(1.02)",
      cursor: "pointer",
    },
  },
  menuItem: {
    width: "unset",
    "&:hover": {
      fontweight: "semi-bold",
      textDecoration: "underline",
    },
  },
  activeMenuItem: {
    width: "unset",
    textDecoration: "underline",
    fontweight: "semi-bold",
  },
}));

const Logo = styled(Typography)(({ theme }) => ({
  // flexGrow: 1,
  cursor: "pointer",
  fontSize: theme.typography.h5.fontSize,
  ":hover": {
    transform: "scale(1.02)",
  },
  [theme.breakpoints.down("sm")]: {
    fontSize: theme.typography.h6.fontSize,
  },
}));


export default function Navigation() {
  const classes = useStyles();
  const theme = useTheme();
  const { asPath } = useRouter();

  return (
    <AppBar
      position="sticky"
      sx={{ bgcolor: blue[50], boxShadow: theme.shadows[2] }}
    >
      <Container maxWidth="lg">
        <Toolbar
          disableGutters
          sx={{
            bgcolor: "transparent",
            justifyContent: "space-between",
            color: theme.palette.primary.main,
          }}
        >
          <Link href={routes.home} passHref>
            <Logo>SOKPHENG KEAN</Logo>
          </Link>
          {/* <MenuIcon aria-label='menu' onClick={() => console.log("Menu Click")} >
                        <MdMenu />
                    </MenuIcon> */}
          <List disablePadding sx={{ display: "flex" }} dense>
            <ListItem disableGutters disablePadding>
              <a
                href={`https://t.me/${process.env.NEXT_PUBLIC_TELEGRAM_USERNAME}`}
                target="_blank"
              >
                <IconButton color="primary" size="small" >
                  <BsTelegram />
                </IconButton>
              </a>
            </ListItem>
            <ListItem disableGutters disablePadding>
              <a
                href={`https://www.facebook.com/${process.env.NEXT_PUBLIC_FACEBOOK_USERNAME}`}
                target="_blank"
              >
                <IconButton color="primary" size="small">
                  <AiFillFacebook />
                </IconButton>
              </a>
            </ListItem>
            <ListItem disableGutters disablePadding>
              <a href="https://gitlab.com/Sokpheng" target="_blank">
                <IconButton color="warning" size="small" >
                  <AiFillGitlab />
                </IconButton>
              </a>
            </ListItem>
          </List>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
