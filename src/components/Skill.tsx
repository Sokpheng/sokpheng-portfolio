import { Box, SxProps, Theme, Typography } from "@mui/material";
import React, { FC } from "react";
import { SiCodesandbox } from "react-icons/si";
import { IconType } from "react-icons/lib";
import { ThemeContext } from "@emotion/react";

interface ISkillProps {
  label: string;
  icon: React.ReactElement<IconType>;
  score: number;
  colors: string;
  sx?: SxProps;
}

const Skill: FC<ISkillProps> = ({ label, icon, colors, score, sx }) => {
  return (
    <Box sx={{ display: "flex", flexDirection: "column", flexGrow: 1, ...sx }}>
      <Box
        sx={{
          display: "flex",
          flexGrow: 1,
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box sx={{ display: "flex", justifyContent: 'flex-end', alignItems: 'center', color: colors }}>
          {icon}
          <Typography variant="h6" sx={{ color: colors, pl: 2 }}>
            {label}
          </Typography>
        </Box>
        <Typography sx={{ color: colors }}>{score}%</Typography>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexGrow: 1,
          mt: 1,
          bgcolor: (theme) => theme.palette.grey[400],
          height: (theme) => theme.spacing(0.5),
        }}
      >
        <Box sx={{ width: `${score}%`, height: "100%", bgcolor: colors }} />
      </Box>
    </Box>
  );
};

export default Skill;
