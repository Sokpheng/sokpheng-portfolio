import React, { FC } from 'react'
import Navigation from './Navigation'

const Layout:FC = ({children}) => {
  return (
    <>
        <Navigation />
        {children}
    </>
  )
}

export default Layout