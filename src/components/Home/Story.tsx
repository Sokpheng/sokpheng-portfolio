import Skill from "@components/Skill";
import { Box, colors, Container, Typography } from "@mui/material";
import Image from "next/image";
import React, { FC } from "react";
import { StoryContainer } from "./styles";
import { SiCodesandbox } from "react-icons/si";
import { MdOutlineMonitor } from "react-icons/md";
import { styled, alpha } from "@mui/material/styles";

const BoxStyled = styled(Box)(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1,
  padding: theme.spacing(4),
  [theme.breakpoints.down("md")]: {
    // display: "none",
    flex: "unset",
    height: 300,
  },
  [theme.breakpoints.down("sm")]: {
    height: 200 + "!important",
  },
  [theme.breakpoints.down("xs")]: {
    height: 100 + "!important",
  },
}));

const ContainerStyled = styled(Container)(({ theme }) => ({
  display: "flex",
  padding: 0,
  [theme.breakpoints.down("md")]: {
    flexDirection: "column",
  }
}))

const Story: FC = () => {
  return (
    <StoryContainer
      sx={{ bgcolor: (theme) => alpha(theme.palette.primary.main, 0.12) }}
    >
      <ContainerStyled>
        <BoxStyled>
          <Box
            sx={{
              flexGrow: 1,
              borderRadius: (theme) => theme.shape.borderRadius,
              overflow: "hidden",
              position: "relative",
            }}
          >
            <Image
              priority={true}
              src={"/static/images/profile.jpg"}
              alt="profile"
              objectFit="cover"
              layout="fill"
            />
          </Box>
        </BoxStyled>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            flex: 1,
            padding: (theme) => theme.spacing(4),
          }}
        >
          <Typography
            variant="h4"
            sx={{
              color: (theme) => theme.palette.primary.main,
              textAlign: "center",
              display: "flex",
              flexDirection: "column",
            }}
          >
            Read My Story
          </Typography>
          <Typography variant="body1" sx={{ mt: (theme) => theme.spacing(4) }}>
            Currently, a Junior Web Developer with one year+ of experience with
            web technology and UX/UI design.
            Ready to take the next journey with a local or remote company to get more
            experience in front-end development. Interested in learning new
            technology, teamwork, and self-manage with I'll try attitudes.
          </Typography>
          <Box sx={{ mt: (theme) => theme.spacing(4) }}>
            <Skill
              colors={colors.green[400]}
              label="Development"
              score={75}
              icon={<SiCodesandbox size={24} />}
            />
            <Skill
              sx={{ mt: 3 }}
              colors={colors.red[400]}
              label="UX/UI Design"
              score={50}
              icon={<MdOutlineMonitor size={24} />}
            />
          </Box>
        </Box>
      </ContainerStyled>
    </StoryContainer>
  );
};

export default Story;
