import { Box, Container, Typography } from "@mui/material";
import { alpha, styled } from "@mui/material/styles";

//#region HERO
export const HeroRoot = styled(Box)(({ theme }) => ({
  backgroundColor: alpha(theme.palette.primary.main, 0.04),
  backgroundRepeat: "no-repeat",
  backgroundSize: 500,
  backgroundPosition: "90% 50%",
  overflow: "hidden",
  [theme.breakpoints.up("md")]: {
    backgroundImage: `url("static/images/hero-bg.png")`,
  },
}));

export const HeroBoxLeft = styled(Box)(({ theme }) => ({
  display: "flex",
  flex: 1,
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "flex-start",
  padding: theme.spacing(16, 0),
  [theme.breakpoints.down("lg")]: {
    padding: theme.spacing(12, 0),
  },
  [theme.breakpoints.down("md")]: {
    padding: theme.spacing(10, 0),
    alignItems: "center",
  },
  [theme.breakpoints.down("sm")]: {
    padding: theme.spacing(5, 0),
  },
  [theme.breakpoints.down("xs")]: {
    padding: theme.spacing(2, 0),
  },
}));

export const HeroHeading = styled(Typography)(({ theme }) => ({
  fontSize: theme.typography.h3.fontSize,
  fontWeight: "bold",
  [theme.breakpoints.down("md")]: {
    fontSize: theme.typography.h4.fontSize,
    textAlign: "center",
  },
  [theme.breakpoints.down("sm")]: {
    fontSize: theme.typography.h5.fontSize,
  },
  [theme.breakpoints.down("xs")]: {
    fontSize: theme.typography.h6.fontSize,
  },
}));

export const HeroDescription = styled(Typography)(({ theme }) => ({
  fontSize: theme.typography.h6.fontSize,
  [theme.breakpoints.down("sm")]: {
    fontSize: theme.typography.subtitle1.fontSize,
  },
}));

//#endregion

//#region STORY
export const StoryContainer = styled(Box)(({ theme }) => ({
  padding: theme.spacing(8, 0),
  flexGrow: 1,
  display: "flex",
  [theme.breakpoints.up("lg")]: {
    padding: theme.spacing(8, 0),
  },
  [theme.breakpoints.down("md")]: {
    padding: theme.spacing(6, 0),
  },
  [theme.breakpoints.down("sm")]: {
    padding: theme.spacing(4, 0),
  },
  [theme.breakpoints.down("xs")]: {
    padding: theme.spacing(2, 0),
  },
}));
//#endregion
