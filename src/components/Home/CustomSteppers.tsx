import * as React from "react";
import { styled } from "@mui/material/styles";
import Stack from "@mui/material/Stack";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import { SxProps, Theme } from "@mui/material";
import { MdSettings, MdOutlineGroup, MdVideoLabel } from "react-icons/md";

import StepConnector, {
  stepConnectorClasses,
} from "@mui/material/StepConnector";
import { StepIconProps } from "@mui/material/StepIcon";
import { Box } from "@mui/material";
import Image from "next/image";
import { blue, blueGrey } from "@mui/material/colors";

const ColorlibConnector = styled(StepConnector)(({ theme }) => ({
  backgroundColor: blueGrey[500],
  top: "calc(50% - 75px / 2 / 2)",
  height: 4,
}));

const ColorlibStepIconRoot = styled("div")(({ theme }) => ({
  zIndex: 1,
  width: 75,
  aspectRatio: "1 / 1",
  display: "flex",
  borderRadius: "50%",
  justifyContent: "center",
  alignItems: "center",
  position: "relative",
  overflow: "hidden",
  backgroundColor: blue[50],
  border: `1px dashed ${theme.palette.primary.main}`,
}));

function ColorlibStepIcon(props: StepIconProps) {
  const icons: { [index: string]: React.ReactElement } = {
    1: <Image src="/static/images/research.png" layout="fill" />,
    2: <Image src="/static/images/analyze.png" layout="fill" />,
    3: <Image src="/static/images/design.png" layout="fill" />,
    4: <Image src="/static/images/coding.png" layout="fill" />,
  };

  return (
    <ColorlibStepIconRoot>{icons[String(props.icon)]}</ColorlibStepIconRoot>
  );
}

const steps = ["Research", "Analyze", "UX/UI Design", "Coding"];

interface IProps {
  sx?: SxProps<Theme>;
}

const StackStyled = styled(Stack)(({ theme }) => ({
  marginTop: theme.spacing(8),
  marginBottom: theme.spacing(8),
  overflowX: "auto",
  "&::-webkit-scrollbar": {
    display: "none",
  },
  [theme.breakpoints.down("md")]: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  [theme.breakpoints.down("sm")]: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

export default function CustomStepper({ sx }: IProps) {
  return (
    <StackStyled sx={{ width: "100%", ...sx }} spacing={4}>
      <Stepper
        alternativeLabel
        activeStep={3}
        connector={<ColorlibConnector />}
      >
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </StackStyled>
  );
}
