import { IEducation } from "@interfaces";
import { Box, Button, Stack, Typography } from "@mui/material";
import { blue } from "@mui/material/colors";
import React from "react";
import RowItem from "./RowItem";
import { AnimatePresence } from "framer-motion";

function Education({ educationData }: { educationData: IEducation[] }) {
  return (
    <Stack
    direction="column"
    spacing={3}
    flexGrow={1}
    >
        <AnimatePresence>
        {educationData.map((item, i) => (
          <RowItem
            key={item.id}
            title={item.title}
            location={item.location}
            btnAction={
              <Button
                key={item.id}
                disableRipple
                disableFocusRipple
                disableTouchRipple
                variant="contained"
                sx={{
                  height: "fit-content",
                  "&:hover": { boxShadow: "none" },
                  opacity: 0.8,
                }}
              >
                {item.year}
              </Button>
            }
          >
            <Typography variant="subtitle1" pl={3} color="gray">
              {item.major}
            </Typography>
          </RowItem>
        ))}
    </AnimatePresence>
      </Stack>
  );
}

export default Education;
