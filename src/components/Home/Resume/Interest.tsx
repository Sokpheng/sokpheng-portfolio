import { IInterest } from "@interfaces";
import { Stack, Typography } from "@mui/material";
import React, { FC } from "react";
import RowItem from "./RowItem";
import { motion } from "framer-motion";
import { varFadeInRight } from "@components/animate";

const Interest = ({ interestData }: { interestData: IInterest[] }) => {
  return (
    <Stack
      direction="column"
      spacing={3}
      flexGrow={1}
    >
      {interestData.map((item, i) => (
        <RowItem title={item.title} key={item.id}></RowItem>
      ))}
    </Stack>
  );
};

export default Interest;
