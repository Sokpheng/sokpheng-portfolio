import {
  Box,
  BoxProps,
  List,
  ListItem,
  ListItemButton,
  ListItemButtonProps,
  ListItemIcon,
  ListItemIconProps,
  ListItemText,
  Stack,
  Typography,
} from "@mui/material";
import React, { FC, useState } from "react";
import { styled, alpha } from "@mui/material/styles";
import { IMenuItem } from "@interfaces";

interface StyledBoxProp extends BoxProps {
  active: "true" | "false";
}

interface StyledListItemButtonProp extends ListItemButtonProps {
  active: "true" | "false";
}

interface StyledListItemIconProp extends ListItemIconProps {
  active: "true" | "false";
}

const BgMenu = styled(Box)<StyledBoxProp>(({ theme, active }) => ({
  backgroundColor: theme.palette.common.black,
  height: "100%",
  position: "absolute",
  top: 0,
  left: 0,
  bottom: 0,
  width: 56,
  ...(active == "true" && {
    width: "100% !important",
  }),
  [theme.breakpoints.down("md")]: {
    borderRadius: 16,
    width: 0,
  },
}));

const ListStyled = styled(List)(({ theme }) => ({
  display: "flex",
  [theme.breakpoints.up("md")]: {
    flexDirection: "column",
  },
}));

const ListItemButtonStyled = styled(ListItemButton)<StyledListItemButtonProp>(
  ({ theme, active }) => ({
    position: "relative",
    borderStartEndRadius: 16,
    borderEndEndRadius: 16,
    overflow: "hidden",
    // paddingRight: 4,
    ...(active == "true" && {
      color: active ? theme.palette.common.white : theme.palette.common.black,
    }),
    [theme.breakpoints.down("md")]: {
      borderStartEndRadius: 16,
      borderEndEndRadius: 16,
      borderStartStartRadius: 16,
      borderEndStartRadius: 16,
      marginLeft: 16,
      minWidth: "fit-content",
      paddingRight: theme.spacing(3),
      backgroundColor: alpha(theme.palette.primary.main, 0.12),
    },
  })
);

const ListItemIconStyled = styled(ListItemIcon)<StyledListItemIconProp>(
  ({ theme, active }) => ({
    color: theme.palette.common.white,
    zIndex: 99,
    [theme.breakpoints.down("md")]: {
      color: theme.palette.common.black,
      minWidth: theme.spacing(4),
      ...(active == "true" && {
        color: theme.palette.common.white,
      })
    },
  })
);

function ResumeMenu({
  menuData,
  activeMenu,
  setActiveMenu,
}: {
  menuData: IMenuItem[];
  activeMenu: IMenuItem;
  setActiveMenu: (menu: IMenuItem) => void;
}) {
  console.log("Menu: ", menuData);
  return (
    <Stack sx={{ position: "relative" }}>
      <ListStyled >
        {menuData.map((item) => (
          <ListItemButtonStyled
            active={item == activeMenu ? "true" : "false"}
            key={item.id}
            selected={item == activeMenu}
            onClick={() => setActiveMenu(item)}
          >
            <BgMenu active={item == activeMenu ? "true" : "false"} />
            <ListItemIconStyled active={item == activeMenu ? "true" : "false"}>
              {item.icon}
            </ListItemIconStyled>
            <ListItemText sx={{ zIndex: 99 }}>{item.label}</ListItemText>
          </ListItemButtonStyled>
        ))}
      </ListStyled>
    </Stack>
  );
}

export default ResumeMenu;
