import { IProgrammingLanguages } from "@interfaces";
import { Box, Stack, Typography } from "@mui/material";
import React, { FC } from "react";
import RowItem from "./RowItem";
import {motion} from "framer-motion";
import { varFadeInRight } from "@components/animate";

const ProgrammingLanguage = ({
  programmingLanguageData,
}: {
  programmingLanguageData: IProgrammingLanguages[];
}) => {
  return (
    <Stack
      direction="column"
      spacing={3}
      flexGrow={1}
    >
      {programmingLanguageData.map((item, i) => (
        <RowItem title={item.name} key={item.id} subLabel={item.level.title}>
          <Stack direction="column" ml={3} flexGrow={1} bgcolor="lightgray">
            <Box
              flexGrow={1}
              height={2}
              sx={{
                bgcolor: (theme) => theme.palette.primary.main,
                width: `${item.level.score}%`,
              }}
            />
          </Stack>
        </RowItem>
      ))}
    </Stack>
  );
};

export default ProgrammingLanguage;
