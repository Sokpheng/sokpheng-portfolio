import { IExperience, IMenuItem } from "@interfaces";
import { Box, Button, Stack, Typography } from "@mui/material";
import React, { FC } from "react";
import RowItem from "./RowItem";
import { motion } from "framer-motion";
import { varFadeInRight } from "@components/animate";

const WorkHistory = ({
  workHistoryData,
}: {
  workHistoryData: IExperience[];
}) => {
  return (
    <Stack
      direction="column"
      spacing={3}
      flexGrow={1}
    >
      {workHistoryData.map((item) => (
        <RowItem
          key={item.id}
          title={item.title}
          location={item.location}
          btnAction={
            <Button
              key={item.id}
              disableRipple
              disableFocusRipple
              disableTouchRipple
              variant="contained"
              sx={{
                height: "fit-content",
                "&:hover": { boxShadow: "none" },
                opacity: 0.8,
              }}
            >
              {item.year}
            </Button>
          }
        >
          <Stack direction="column" ml={3} spacing={1}>
            {item.position.map((position) => (
              <Box component={"ul"} p={0} m={0} key={position.id}>
                <Typography variant="subtitle1" fontWeight="bold">
                  {position.title}
                </Typography>
                {position.responsibilities.map((responsibility, i) => (
                  <Box component={"li"} ml={3} key={i}>
                    {responsibility}
                  </Box>
                ))}
              </Box>
            ))}
          </Stack>
        </RowItem>
      ))}
    </Stack>
  );
};

export default WorkHistory;
