import { Stack, Typography } from '@mui/material';
import React, { FC } from 'react'
import { GoPrimitiveDot } from 'react-icons/go';
import {motion} from 'framer-motion';
import { varFadeIn, varFadeInRight } from '@components/animate';

interface IRowItem {
    title: string;
    location?: string;
    btnAction?: React.ReactNode;
    subLabel?: string;
}

const RowItem:FC<IRowItem> = ({title, children, btnAction, location, subLabel}) => {
  return (
    <Stack
      direction="row"
      justifyContent="space-between"
      component={motion.div}
      {...varFadeIn}
    >
      <Stack direction="column" flexGrow={1}>
        <Stack flexGrow={1} direction="row" justifyContent="space-between">
          <Stack
            flexGrow={1}
            direction="row"
            justifyContent="flex-start"
            spacing={1}
            alignItems="center"
            sx={{ color: (theme) => theme.palette.primary.main }}
          >
            <Stack direction="row" alignItems="flex-start" flexGrow={1} >
              <Stack mt={0.5} >
                <GoPrimitiveDot />
              </Stack>
              <Typography variant="body1" sx={{ fontWeight: "bold", pl: 1 }}>
                {title} {location}
              </Typography>
            </Stack>
            {subLabel && (
              <Typography variant="subtitle2">{subLabel}</Typography>
            )}
          </Stack>
          {btnAction}
        </Stack>
        {children}
      </Stack>
    </Stack>
  );
}

export default RowItem