import WrapperContainer from "@components/WrapperContainer";
import { Button, Divider, Grid, Stack } from "@mui/material";
import { grey } from "@mui/material/colors";
import { Box, padding } from "@mui/system";
import React, { FC, useEffect, useState } from "react";
import Education from "./Education";
import ResumeMenu from "./ResumeMenu";
import { AiOutlineDownload } from "react-icons/ai";
import { menuData, resumeData } from "@dummy";
import { IMenuItem } from "@interfaces";
import WorkHistory from "./WorkHistory";
import ProgrammingLanguage from "./ProgrammingLanguage";
import Project from "./Project";
import Interest from "./Interest";
import { styled, alpha } from "@mui/material/styles";

const StackStyled = styled(Stack)(({ theme }) => ({
  [theme.breakpoints.up("md")]: {
    flexDirection: "row",
  },
}));


const StyledGrid = styled(Grid)(({ theme }) => ({
  marginTop: theme.spacing(8),
  marginBottom: theme.spacing(8),
  [theme.breakpoints.down("md")]: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  [theme.breakpoints.down("sm")]: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  }
}));

const Resume: FC = () => {
  const [activeMenu, setActiveMenu] = useState<IMenuItem>(menuData[0]);

  const activeContent = () => {
    switch (activeMenu.id) {
      case 1:
        return <Education educationData={resumeData.education} />;
      case 2:
        return <WorkHistory workHistoryData={resumeData.experience} />;
      case 3:
        return (
          <ProgrammingLanguage
            programmingLanguageData={resumeData.programmingLanguages}
          />
        );
      case 4:
        return <Project projectData={resumeData.projects} />;
      case 5:
        return <Interest interestData={resumeData.interests} />;
    }
  };

  return (
    <WrapperContainer
      title="Resume"
      label="My formal bio details"
      sx={{ bgcolor: (theme) => alpha(theme.palette.primary.main, 0.12) }}
    >
      <StyledGrid container spacing={2}>
        <Grid item xs={12} md={3} sx={{ overflowX: "auto" }}>
          <ResumeMenu
            menuData={menuData}
            activeMenu={activeMenu}
            setActiveMenu={(item) => setActiveMenu(item)}
          />
        </Grid>
        <Grid item xs={12} md={1}>
          <Box
            sx={{
              flexGrow: 1,
              height: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Box sx={{ width: 2, height: "100%", bgcolor: "lightgrey" }} />
          </Box>
        </Grid>
        <Grid item xs={12} md={8}>
          {activeContent()}
        </Grid>
      </StyledGrid>
      <Box
        sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}
      >
        <a href="/static/attachments/sokpheng-cv.pdf" download>
          <Button startIcon={<AiOutlineDownload />} variant="contained">
            Get Resume
          </Button>
        </a>
      </Box>
    </WrapperContainer>
  );
};

export default Resume;
