import { IProject } from "@interfaces";
import { Box, Button, Stack, Typography } from "@mui/material";
import Link from "next/link";
import React, { FC } from "react";
import RowItem from "./RowItem";
import {motion} from "framer-motion";
import { varFadeInRight } from "@components/animate";

const Project = ({ projectData }: { projectData: IProject[] }) => {
  return (
    <Stack
      direction="column"
      spacing={3}
      flexGrow={1}
    >
      {projectData.map((item, i) => (
        <RowItem
          title={item.title}
          key={item.id}
          btnAction={
            <Stack direction="row" spacing={1}>
              {item.website?.map((item, i) => (
                <Link key={i} href={item.url}>
                  <Button
                    color="secondary"
                    variant="contained"
                    sx={{ height: "fit-content", opacity: 0.8 }}
                  >
                    {item.text}
                  </Button>
                </Link>
              ))}
            </Stack>
          }
        >
          <Stack direction="column" ml={3}>
            <Typography variant="subtitle1" fontWeight="bold">
              Technology used:{" "}
              {item.technologies.map((tech, i) => (
                <span key={i}>{i == 0 ? tech : ", " + tech}</span>
              ))}
            </Typography>
            <Typography variant="subtitle1">{item.description}</Typography>
          </Stack>
        </RowItem>
      ))}
    </Stack>
  );
};

export default Project;
