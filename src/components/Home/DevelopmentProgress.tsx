import React, { FC } from "react";
import WrapperContainer from "@components/WrapperContainer";
import { alpha } from "@mui/material/styles";
import { colors } from "@mui/material";
import CustomStepper from "./CustomSteppers";

const DevelopmentProgress: FC = () => {
  return (
    <WrapperContainer
      title="My Development Progress"
      label="Step of completing the devlopment process"
      sx={{ bgcolor: (theme) => alpha(theme.palette.primary.main, 0.04) }}
    >
      <CustomStepper />
    </WrapperContainer>
  );
};

export default DevelopmentProgress;
