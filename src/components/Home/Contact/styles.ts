import { Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

export const StackStyled = styled(Stack)(({ theme }) => ({
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  [theme.breakpoints.down("md")]: {
    marginTop: theme.spacing(6),
  },
}));
