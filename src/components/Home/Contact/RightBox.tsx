import { Box, Button, Grid, Stack, TextField, Typography } from '@mui/material';
import React from 'react'
import {RiSendPlaneFill} from 'react-icons/ri'
import { styled } from '@mui/material/styles';
import { StackStyled } from './styles';

const RightBox = () => {
  return (
    <StackStyled>
      <Typography
        variant="h5"
        sx={{ color: (theme) => theme.palette.primary.main }}
      >
        Get In Touch
      </Typography>
      <Stack
        component="form"
        action={`mailto:${process.env.NEXT_PUBLIC_EMAIL}`}
        method="post"
        encType="text/plain"
        mt={4}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                <TextField
                  name="fullName"
                  label="Enter full name"
                  required
                  variant="outlined"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  name="email"
                  label="Email"
                  required
                  variant="outlined"
                  fullWidth
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <TextField label="Subject" required variant="outlined" fullWidth />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="body"
              label="Body"
              required
              variant="outlined"
              fullWidth
              multiline
              rows={4}
            />
          </Grid>
        </Grid>
        <Stack
          flexGrow={1}
          justifyContent="flex-end"
          alignItems="flex-end"
          mt={2}
        >
          <Box>
            <Button
              type="submit"
              startIcon={<RiSendPlaneFill />}
              variant="contained"
            >
              Sent Now
            </Button>
          </Box>
        </Stack>
      </Stack>
    </StackStyled>
  );
}

export default RightBox