import WrapperContainer from "@components/WrapperContainer";
import { Button, Grid, Stack } from "@mui/material";
import Image from "next/image";
import React, { FC } from "react";
import { BsTelegram, BsFacebook } from "react-icons/bs";
import LeftBox from "./LeftBox";
import RightBox from "./RightBox";

const Contact: FC = () => {
  return (
    <WrapperContainer
      title="Contact Me"
      label="Feel free to contact me any time."
    >
      <Stack flexGrow={1}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <LeftBox />
          </Grid>
          <Grid item xs={12} md={6}>
            <RightBox />
          </Grid>
        </Grid>
      </Stack>
    </WrapperContainer>
  );
};

export default Contact;
