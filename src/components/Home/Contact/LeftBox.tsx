import { Button, Stack } from '@mui/material';
import Image from 'next/image';
import React from 'react'
import { BsFacebook, BsTelegram } from 'react-icons/bs';
import { StackStyled } from './styles';

const LeftBox = () => {
  return (
    <Stack alignItems="center" justifyContent="center" flex={1}>
      <Image src="/static/images/contact.png" width={200} height={200} />
      <Stack
        mt={10}
        direction="row"
        width="100%"
        justifyContent="center"
        spacing={2}
      >
        <a
          href={`https://t.me/${process.env.NEXT_PUBLIC_TELEGRAM_USERNAME}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Button startIcon={<BsTelegram />} variant="contained" color='warning' >
            Telegram
          </Button>
        </a>
        <a
          href={`https://www.facebook.com/${process.env.NEXT_PUBLIC_FACEBOOK_USERNAME}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Button startIcon={<BsFacebook />} variant="contained">
            Facebook
          </Button>
        </a>
      </Stack>
    </Stack>
  );
}

export default LeftBox