import { Box, Button, Container, SxProps, Theme, Typography, BoxProps, Stack } from '@mui/material'
import { makeStyles } from '@mui/styles';
import { styled, alpha } from '@mui/material/styles';
import React, { FC } from 'react'
import { HeroBoxLeft, HeroDescription, HeroHeading, HeroRoot } from './styles';
import Typical from 'react-typical'
import { heroTypical } from '@constants/mock-data';
import { FaPen } from 'react-icons/fa';
import { AiOutlineDownload } from 'react-icons/ai';
import { BsTelegram } from 'react-icons/bs';

interface StyledBoxProp extends BoxProps {
  active?: boolean
}

const TestStyled = styled(Box)<StyledBoxProp>(({active, theme}) => ({
  backgroundColor: "blue",
  ...(active && {
    backgroundColor: "red"
  }),
}))

const Hero: FC = () => {
  return (
    <HeroRoot>
      <Container
        maxWidth="lg"
        sx={{ display: "flex", flexGrow: 1, minHeight: "50vh" }}
      >
        <HeroBoxLeft>
          <HeroHeading variant="h3">
            HEY, I AM
            <HeroHeading
              sx={{
                color: (theme) => theme.palette.primary.main,
                marginTop: 1,
              }}
            >
              <Typical steps={heroTypical} loop={Infinity} wrapper="span" />
            </HeroHeading>
          </HeroHeading>
          <HeroDescription maxWidth={500} marginY={4}>
            I am a Junior Developer with a passion for building web
            applications. I have a strong passion for learning new technologies
            and constantly exploring new ways to solve problems.
          </HeroDescription>
          <Stack direction="row" spacing={2}>
            <a
              href="/static/attachments/sokpheng-cv.pdf"
              target="_blank"
              rel="noopener noreferrer"
              download
            >
              <Button startIcon={<AiOutlineDownload />} variant="contained">
                Get Resume
              </Button>
            </a>
            <a
              href={`https://t.me/${process.env.NEXT_PUBLIC_TELEGRAM_USERNAME}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Button startIcon={<BsTelegram />} variant="contained" color='warning' >
                Telegram
              </Button>
            </a>
          </Stack>
        </HeroBoxLeft>
      </Container>
    </HeroRoot>
  );
}

export default Hero