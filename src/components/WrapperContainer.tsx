import { Box, Container, Stack, SxProps, Typography } from "@mui/material";
import React, { FC } from "react";
import { styled, alpha } from "@mui/material/styles";
import {Theme} from '@mui/material'

interface Props {
  title: string;
  label: string;
  children: React.ReactNode;
  sx?: SxProps<Theme>;
  sxContianer?: SxProps<Theme>;
}

const ContainerStyled = styled(Box)(({ theme }) => ({
  padding: theme.spacing(8, 0),
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  [theme.breakpoints.down("md")]: {
    padding: theme.spacing(6, 0),
  },
  [theme.breakpoints.down("sm")]: {
    padding: theme.spacing(4, 0),
  },
  [theme.breakpoints.down("xs")]: {
    padding: theme.spacing(2, 0),
  },
}));

const BoxStyled = styled(Box)(({ theme }) => ({
  marginTop: theme.spacing(4),
  [theme.breakpoints.down("md")]: {
    marginTop: theme.spacing(2),
  }
}))

const LabelLine = styled(Box)(({ theme }) => ({
  height: 1,
  flexGrow: 1,
  backgroundColor: alpha(theme.palette.common.black, 0.12),
}))

const WrapperContainer: FC<Props> = ({ title, children, label, sx }) => {
  return (
    <ContainerStyled sx={{ ...sx }}>
      <Container maxWidth="lg">
        <Stack spacing={1} justifyContent="center" sx={{ flexGrow: 1 }}>
          <Typography
            variant="h4"
            sx={{
              color: (theme) => theme.palette.primary.main,
              textAlign: "center",
              flexGrow: 1,
            }}
          >
            {title}
          </Typography>
          <Stack
            direction="row"
            spacing={1}
            justifyContent="center"
            alignItems="center"
          >
            <LabelLine />
            <Typography variant="body1" color={theme => theme.palette.grey[500]} >{label}</Typography>
            <LabelLine />
          </Stack>
        </Stack>

        <BoxStyled>{children}</BoxStyled>
      </Container>
    </ContainerStyled>
  );
};

export default WrapperContainer;
