interface IRoutes {
    home: string,
    about: string,
    resume: string
}

export const routes:IRoutes = {
    home: '/',
    about: "/about",
    resume: "/project"
}