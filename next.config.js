const withPlugins = require("next-compose-plugins");

module.exports = withPlugins([], {
  reactStrictMode: true,
  env: {
    API_URL: "http://localhost:3000",
    APP_NAME: "Sokpheng's Portfolio",
  }
});
